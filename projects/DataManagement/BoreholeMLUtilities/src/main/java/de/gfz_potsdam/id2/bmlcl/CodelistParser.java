/*
 * Copyright (C) 2020
 * Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; even without the implied WARRANTY OF MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program (see LICENSE.txt). If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA or
 * visit the Free Software Foundation's web page, http://www.fsf.org.
 */
package de.gfz_potsdam.id2.bmlcl;

import de.infogeo.boreholeml.x30.codelists.BMLMLCodeDefinitionDocument;
import de.infogeo.boreholeml.x30.codelists.BMLMLCodeDefinitionType;

import java.io.IOException;
import java.io.InputStream;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import net.opengis.gml.x32.CodeType;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;

import org.isotc211.x2005.gmx.CodeAlternativeExpressionPropertyType;
import org.isotc211.x2005.gmx.CodeAlternativeExpressionType;
import org.isotc211.x2005.gmx.CodeDefinitionPropertyType;
import org.isotc211.x2005.gmx.MLCodeListDictionaryDocument;
import org.isotc211.x2005.gmx.MLCodeListDictionaryType;

/**
 * <p>
 * Title:</p>
 *
 * <p>
 * Description: </p>
 *
 * Copyright (c) Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum,
 * 2021-04-12
 *
 *
 * @version 1.0, 2021-04-12
 * @author Rainer Haener
 */
public class CodelistParser {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CodelistParser codelistParser = new CodelistParser();
//        String file = "codelists/ChronoStratigraphyList.xml";
//        String file = "codelists/DrillingPurposeList.xml";
//        String file = "codelists/SampleTypeList.xml";
//        String file = "codelists/SamplingPurposeList.xml";
        String file = "codelists/DrillingMethodList.xml";
//        String file = "codelists/RockNameList.xml";

        try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(file)) {
            MLCodeListDictionaryDocument document = MLCodeListDictionaryDocument.Factory.parse(is);

            Map<String, List<String>> codeList = codelistParser.parseCodeList(document);

            codeList.forEach((k, v) -> {

                System.out.println(k + ":" + v);
            });
        } catch (IOException | XmlException ex) {
            Logger.getLogger(CodelistParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Method description
     *
     *
     * @param codelist
     *
     * @return
     *
     * @throws IOException
     * @throws XmlException
     */
    public Map<String, List<String>> parseCodeList(String codelist) throws XmlException, IOException {
        Map<String, List<String>> identifierMap = new HashMap<>();
        String file = "codelists/" + codelist + ".xml";

        try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(file)) {
            MLCodeListDictionaryDocument document = MLCodeListDictionaryDocument.Factory.parse(is);

            return parseCodeList(document);
        } catch (IOException | XmlException ex) {
            Logger.getLogger(CodelistParser.class.getName()).log(Level.SEVERE, null, ex);
        }

        return identifierMap;
    }

    /**
     * Method description
     *
     *
     * @param document
     * @return
     *
     * @throws IOException
     * @throws XmlException
     */
    public Map<String, List<String>> parseCodeList(MLCodeListDictionaryDocument document) throws XmlException, IOException {
        Map<String, List<String>> identifierMap = new HashMap<>();
        MLCodeListDictionaryType dictionary = document.getMLCodeListDictionary();

        CodeDefinitionPropertyType[] codeEntryArray = dictionary.getCodeEntryArray();

        for (CodeDefinitionPropertyType cdpt : codeEntryArray) {
            XmlCursor cdCursor = cdpt.newCursor();
            cdCursor.toFirstChild();

            BMLMLCodeDefinitionDocument parse = BMLMLCodeDefinitionDocument.Factory.parse(cdCursor.getDomNode());
            BMLMLCodeDefinitionType codeDefinition = parse.getBMLMLCodeDefinition();
            cdCursor.dispose();
            CodeType[] nameArray = codeDefinition.getNameArray();
            Stream<CodeType> nameStream = Stream.of(nameArray);

//            nameStream.forEach(
//                    (CodeType ct) -> {
//                        System.out.println(ct.getStringValue() + " id: " + codeDefinition.getIdentifier().getStringValue());
//                    });
            CodeAlternativeExpressionPropertyType alternativeExpression = codeDefinition.getAlternativeExpressionArray(0);
            CodeAlternativeExpressionType codeAlternativeExpression = alternativeExpression.getCodeAlternativeExpression();
            String code = codeAlternativeExpression.getIdentifier().getStringValue();
            CodeType[] alterNameArray = codeAlternativeExpression.getNameArray();
            List<String> nameList = new ArrayList<>();

            for (CodeType ct : alterNameArray) {
                nameList.add(ct.getStringValue());
            }

            identifierMap.put(code, nameList);
        }

        return identifierMap;
    }
}

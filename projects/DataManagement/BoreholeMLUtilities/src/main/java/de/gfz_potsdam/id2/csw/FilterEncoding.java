/*
 * Copyright (C) 2016
 * Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; even without the implied WARRANTY OF MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program (see LICENSE.txt). If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA or
 * visit the Free Software Foundation's web page, http://www.fsf.org.
 */
package de.gfz_potsdam.id2.csw;

import net.opengis.cat.csw.x202.ElementSetNameType;
import net.opengis.cat.csw.x202.GetRecordsDocument;
import net.opengis.cat.csw.x202.GetRecordsType;
import net.opengis.cat.csw.x202.QueryDocument;
import net.opengis.cat.csw.x202.QueryType;
import net.opengis.ogc.FilterDocument;
import net.opengis.ogc.FilterType;
import net.opengis.ogc.LogicOpsType;


/**
 * <p>
 * Title:</p>
 *
 * <p>
 * Description: </p>
 *
 * Copyright (c) Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum,
 * 2019-07-25
 *
 *
 * @version 0.9, 2019-07-25
 * @author Rainer Haener
 */
public class FilterEncoding {
    
    public static void main (String [] args) {
        FilterEncoding filterEncoding = new FilterEncoding ();
        filterEncoding.getRecords();
    }

    /**
     * Method description
     *
     */
    public void getRecords() {
        GetRecordsDocument getRecordsDocument = GetRecordsDocument.Factory.newInstance();
        GetRecordsType getRecordsType = getRecordsDocument.addNewGetRecords();

        QueryDocument queryDocument = QueryDocument.Factory.newInstance();
        QueryType queryType = queryDocument.addNewQuery();

        
        
        ElementSetNameType elementSetNameType = queryType.addNewElementSetName();

        elementSetNameType.setStringValue("full");
        getRecordsType.setAbstractQuery(queryType);
        
        
        System.out.println(getRecordsDocument.toString());
    }

    /**
     * Method description
     *
     */
    public void create() {
        FilterDocument filterDocument = FilterDocument.Factory.newInstance();
        FilterType filterType = filterDocument.addNewFilter();
        LogicOpsType logicOpsType = filterType.addNewLogicOps();

//      logicOpsType.set(filterType)
    }
}

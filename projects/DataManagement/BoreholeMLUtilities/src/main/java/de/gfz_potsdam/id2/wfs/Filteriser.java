/*
 * Copyright (C) 2016
 * Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; even without the implied WARRANTY OF MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program (see LICENSE.txt). If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA or
 * visit the Free Software Foundation's web page, http://www.fsf.org.
 */
package de.gfz_potsdam.id2.wfs;

import java.io.IOException;
import java.io.InputStream;

import java.util.logging.Level;
import java.util.logging.Logger;
import net.opengis.ogc.BinaryLogicOpType;
import net.opengis.ogc.ComparisonOpsType;
import net.opengis.ogc.FilterDocument;
import net.opengis.ogc.FilterType;
import net.opengis.ogc.LogicOpsType;
import net.opengis.ogc.OrDocument;
import org.apache.xmlbeans.SchemaType;
import org.apache.xmlbeans.XmlException;

/**
 * <p>
 * Title:</p>
 *
 * <p>
 * Description: </p>
 *
 * Copyright (c) Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum,
 * 2020-03-02
 *
 *
 * @version 0.9, 2020-03-02
 * @author Rainer Haener
 */
public class Filteriser {

    /**
     * Field description
     */
    private FilterDocument filterDocument;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Filteriser filteriser = new Filteriser();
        filteriser.filter();
    }

    /**
     * Method description
     *
     */
    public void filter() {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("samples/filter/during.xml")) {
            filterDocument = FilterDocument.Factory.parse(inputStream);

            FilterType filter = filterDocument.getFilter();
            OrDocument od;

//            LogicOpsType logicOps = filter.getLogicOps();
//            ComparisonOpsType comparisonOps = filter.getComparisonOps();
            BinaryLogicOpType blot = (BinaryLogicOpType) filter.getLogicOps();
            SchemaType schemaType;

//            TemporalOpsType[] temporalOpsArray = blot.getTemporalOpsArray();
//            for (TemporalOpsType temporalOps : temporalOpsArray) {
//                schemaType = temporalOps.schemaType();
//                int compareTo = temporalOps.compareValue(temporalOps.newCursor().getObject());
//                System.out.println("Temporal " + schemaType.getName() + " cmp " + compareTo);
//
//            }
            LogicOpsType[] logicOpsArray = blot.getLogicOpsArray();
            for (LogicOpsType logicOps : logicOpsArray) {
                schemaType = logicOps.schemaType();
                System.out.println(schemaType.getName());

            }
            ComparisonOpsType[] comparisonOpsArray = blot.getComparisonOpsArray();
            for (ComparisonOpsType comparisonOps : comparisonOpsArray) {
                schemaType = comparisonOps.schemaType();
                System.out.println(schemaType.getName());

            }
//            TemporalOpsType temporalOps = filter.getTemporalOps();

        } catch (IOException ex) {
            Logger.getLogger(Filteriser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (XmlException ex) {
            Logger.getLogger(Filteriser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

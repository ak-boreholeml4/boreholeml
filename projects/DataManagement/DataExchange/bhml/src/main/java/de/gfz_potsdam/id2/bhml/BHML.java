/*
 * Copyright (C) 2016
 * Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; even without the implied WARRANTY OF MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program (see LICENSE.txt). If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA or
 * visit the Free Software Foundation's web page, http://www.fsf.org.
 */
package de.gfz_potsdam.id2.bhml;

import com.githubusercontent.raw.opengeospatial.boreholeie.master.schemas.AbstractLocatedMemberType;
import com.githubusercontent.raw.opengeospatial.boreholeie.master.schemas.BoreholeDocument;
import com.githubusercontent.raw.opengeospatial.boreholeie.master.schemas.BoreholeEventCollectionType;
import com.githubusercontent.raw.opengeospatial.boreholeie.master.schemas.BoreholeEventCollectionType.CollectionMember;
import com.githubusercontent.raw.opengeospatial.boreholeie.master.schemas.BoreholeEventLocationType;
import com.githubusercontent.raw.opengeospatial.boreholeie.master.schemas.BoreholeEventType;
import com.githubusercontent.raw.opengeospatial.boreholeie.master.schemas.BoreholeType;
import com.githubusercontent.raw.opengeospatial.boreholeie.master.schemas.FromToLocationType;
import com.githubusercontent.raw.opengeospatial.boreholeie.master.schemas.LocationType;

import de.gfzPotsdam.boreholeml.x42.ChronoStratigraphyType;
import de.gfzPotsdam.boreholeml.x42.LocatedRockClassificationMemberType;
import de.gfzPotsdam.boreholeml.x42.RockCodeDocument;
import de.gfzPotsdam.boreholeml.x42.StratigraphyPropertyType;

import java.io.IOException;
import java.io.InputStream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import net.opengis.gml.x32.CodeType;
import net.opengis.gml.x32.ReferenceType;
import net.opengis.gsml.x41.geoSciMLBasic.CompositionPartPropertyType;
import net.opengis.gsml.x41.geoSciMLBasic.CompositionPartType;
import net.opengis.gsml.x41.geoSciMLBasic.CompoundMaterialType;
import net.opengis.gsml.x41.geoSciMLBasic.ContactDocument;
import net.opengis.gsml.x41.geoSciMLBasic.ContactType;
import net.opengis.gsml.x41.geoSciMLBasic.GeologicUnitDocument;
import net.opengis.gsml.x41.geoSciMLBasic.GeologicUnitType;
import net.opengis.gsml.x41.geoSciMLBasic.RockMaterialType;
import net.opengis.swe.x20.AbstractDataComponentType;
import net.opengis.swe.x20.DataRecordDocument;
import net.opengis.swe.x20.DataRecordType;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlException;

/**
 * <p>
 * Title:</p>
 *
 * <p>
 * Description: </p>
 *
 * Copyright (c) Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum,
 * 2019-10-24
 *
 *
 * @version 0.9, 2019-10-24
 * @author Rainer Haener
 */
public class BHML {

    /**
     * Field description
     */
    private static final String NULL_MATERIAL = "no material provided";

    /**
     * Field description
     */
    private final String GSML_NAMESPACE = "declare namespace gsml='http://www.opengis.net/gsml/4.1/GeoSciML-Basic';";

    /**
     * Field description
     */
    private final String XLINK_NAMESPACE = "declare namespace xlink='http://www.w3.org/1999/xlink';";

    /**
     * Field description
     */
    private BoreholeDocument document;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BHML bhml = new BHML();

        bhml.streamIt();

//      bhml.sweIt();
    }

    /**
     * Method description
     *
     */
    public void load() {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("samples/nrw_dabo18018.xml")) {
            document = BoreholeDocument.Factory.parse(inputStream);

            BoreholeType borehole = document.getBorehole();
            BoreholeType.Collection[] collections = borehole.getCollectionArray();

            for (BoreholeType.Collection collection : collections) {
                BoreholeEventCollectionType boreholeEventCollection = collection.getBoreholeEventCollection();
                BoreholeEventCollectionType.CollectionMember[] collectionMembers = boreholeEventCollection.getCollectionMemberArray();

                for (BoreholeEventCollectionType.CollectionMember collectionMember : collectionMembers) {
                    BoreholeEventType boreholeEvent = collectionMember.getBoreholeEvent();
                    LocationType location = boreholeEvent.getLocation2();
                    BoreholeEventLocationType boreholeEventLocation = location.getBoreholeEventLocation();
                    String localPart = boreholeEventLocation.schemaType().getName().getLocalPart();

                    if ("FromToLocationType".equals(localPart)) {
                        FromToLocationType fromToLocationType = (FromToLocationType) location.getBoreholeEventLocation();

//                      System.out.println(fromToLocationType.getFromPosition().getMeasureOrEvent().getDistanceAlong().getDoubleValue() + " <=> " + fromToLocationType.getToPosition().getMeasureOrEvent().getDistanceAlong().getDoubleValue());
                    }

                    AbstractLocatedMemberType locatedMember = boreholeEvent.getLocatedMember();
                    String qname = locatedMember.schemaType().getName().getLocalPart();

                    if ("LocatedRockClassificationMemberType".equalsIgnoreCase(qname)) {
                        LocatedRockClassificationMemberType locatedRockClassificationMember = (LocatedRockClassificationMemberType) boreholeEvent.getLocatedMember();
                        RockCodeDocument.RockCode rockCode = locatedRockClassificationMember.getRockCode();

//                      System.out.println("\trock code " + rockCode.getStringValue());
                        StratigraphyPropertyType stratigraphy = locatedRockClassificationMember.getStratigraphy();

                        if (stratigraphy != null) {
                            ChronoStratigraphyType chronoStratigraphy = stratigraphy.getChronoStratigraphy();

                            if (chronoStratigraphy != null) {

//                              System.out.println("\tchronoStratigraphy " + chronoStratigraphy.getStringValue());
                            }
                        }
                    }
                }
            }
        } catch (XmlException | IOException ex) {
            Logger.getLogger(BHML.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Method description
     *
     */
    public void streamIt() {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("samples/CeRDI_Borehole.xml")) {
            document = BoreholeDocument.Factory.parse(inputStream);

            BoreholeType borehole = document.getBorehole();
            BoreholeType.Collection[] collections = borehole.getCollectionArray();

            for (BoreholeType.Collection collection : collections) {
                BoreholeEventCollectionType boreholeEventCollection = collection.getBoreholeEventCollection();
                BoreholeEventCollectionType.CollectionMember[] collectionMembers = boreholeEventCollection.getCollectionMemberArray();

                this.collectIt(collectionMembers);
            }
        } catch (XmlException | IOException ex) {
            Logger.getLogger(BHML.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Method description
     *
     *
     * @param collectionMembers
     */
    public void collectIt(final BoreholeEventCollectionType.CollectionMember[] collectionMembers) {
        List<BoreholeEventCollectionType.CollectionMember> cms = Arrays.asList(collectionMembers);
        Stream<Optional<String>> map = cms.stream().map(new Function<CollectionMember, Optional<String>>() {
            @Override
            public Optional<String> apply(final CollectionMember t) {
                BoreholeEventType boreholeEvent = t.getBoreholeEvent();
                AbstractLocatedMemberType locatedMember = boreholeEvent.getLocatedMember();
                String qname = locatedMember.schemaType().getName().getLocalPart();

                System.out.println("qname " + qname);

                XmlCursor locatedMemberCursor = locatedMember.newCursor();

                locatedMemberCursor.toFirstChild();

                StringBuilder strbui = new StringBuilder();
                String name = null;

                qname = locatedMemberCursor.getName().getLocalPart();

                List<Lithology> lithologies = new ArrayList<>();

                if ("GeologicUnit".equalsIgnoreCase(qname)) {
                    try {
                        GeologicUnitDocument geologicUnitDocumentDocument = GeologicUnitDocument.Factory.parse(locatedMemberCursor.getDomNode());
                        GeologicUnitType geologicUnit = geologicUnitDocumentDocument.getGeologicUnit();
                        CompositionPartPropertyType[] compositionArray = geologicUnit.getCompositionArray();

                        // empty arry in case not composition element is present
                        for (CompositionPartPropertyType composition : compositionArray) {
                            Optional<String> material = getMaterial(composition);
                            Lithology l = new Lithology();

                            l.setMaterial(material.get());
                            lithologies.add(l);
                        }
                    } catch (XmlException ex) {
                        Lithology l = new Lithology();

                        l.setMaterial(Optional.ofNullable(name).get());

                        return Optional.ofNullable(name);
                    }
                }

                lithologies.forEach(
                        l -> {
                            System.out.println("Lithology " + l.material);
                        });

                if ("Contact".equalsIgnoreCase(qname)) {
                    try {
                        ContactDocument contactDocument = ContactDocument.Factory.parse(locatedMemberCursor.getDomNode());
                        ContactType contact = contactDocument.getContact();

                        qname = contact.schemaType().getName().getLocalPart();

                        CodeType[] nameArray = contact.getNameArray();

                        for (CodeType codeType : nameArray) {
                            strbui.append(codeType.getStringValue() + ", ");
                        }

                        name = strbui.toString();
                    } catch (XmlException ex) {

                        // return Optional.ofNullable(name);
                    }
                }

                locatedMemberCursor.dispose();

                return Optional.ofNullable(name);

                // if ("LocatedRockClassificationMemberType".equalsIgnoreCase(qname)) {
                // LocatedRockClassificationMemberType locatedRockClassificationMember = (LocatedRockClassificationMemberType) boreholeEvent.getLocatedMember();
                // RockCodeDocument.RockCode rockCode = locatedRockClassificationMember.getRockCode();
                // StratigraphyPropertyType stratigraphy = locatedRockClassificationMember.getStratigraphy();
                //
                // if (stratigraphy != null) {
                // ChronoStratigraphyType chronoStratigraphy = stratigraphy.getChronoStratigraphy();
                //
                // if (chronoStratigraphy != null) {
                // cs = chronoStratigraphy.getStringValue();
                // }
                // }
                // }
            }
        });

        map.forEach(
                (Optional<String> cs) -> {
                    System.out.println(cs.orElse("not provided"));
                });
    }

    /**
     * Method description
     *
     *
     *
     * @param composition
     * @return
     */
    Optional<String> getMaterial(final CompositionPartPropertyType composition) {
        CompositionPartType.Material material = composition.getCompositionPart().getMaterial();
        String queryExpression = "let $e := $this//gsml:lithology[@xlink:title]" + "return $e";
        XmlCursor compositionCursor = composition.newCursor();

        compositionCursor.toFirstChild();

//      compositionCursor.execQuery(XLINK_NAMESPACE + GSML_NAMESPACE + queryExpression);
        String query1 = "//gsml:RockMaterial//gsml:lithology[@xlink:title]";
        String query = "$this//gsml:lithology[@xlink:title]";

//      String query = "$this//gsml:lithology/@xlink:title";
//      String query = "$this//gsml:lithology[@xlink:title='BRN FS LOAM DRY']";
        compositionCursor.selectPath(XLINK_NAMESPACE + GSML_NAMESPACE + query);

        boolean toNextSelection = compositionCursor.toNextSelection();

        if (toNextSelection) {
            System.out.println("\ttv \"" + ((ReferenceType) compositionCursor.getObject()).getTitle());

//          System.out.println("\ttv \"" + compositionCursor.getTextValue() + "\"");
        }

        compositionCursor.dispose();

        if (material == null) {
            return Optional.ofNullable(NULL_MATERIAL);
        }

        CompoundMaterialType compoundMaterial = material.getCompoundMaterial();
        RockMaterialType rockMaterial = (RockMaterialType) material.getCompoundMaterial();

        if (rockMaterial == null) {
            return Optional.ofNullable(NULL_MATERIAL);
        }

        ReferenceType lithology = rockMaterial.getLithology();

//      if ((lithology != null) && (title = lithology.getTitle()) != null) {
//          return Optional.ofNullable(title);
//      }
        if ((lithology == null)) {
            return Optional.ofNullable(NULL_MATERIAL);
        }

        // finally we got something (an xlink:titls !!!!!) what represents the lithology
        String title = lithology.getTitle();

        // anyhow this might be not present
        if (title == null) {
            return Optional.ofNullable(NULL_MATERIAL);
        }

        // finally
        return Optional.ofNullable(title);
    }

    /**
     * Method description
     *
     */
    public void sweIt() {
        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("samples/nrw_dabo18018_swe.xml")) {
            DataRecordDocument dataRecordDocument = DataRecordDocument.Factory.parse(inputStream);
            DataRecordType dataRecord = dataRecordDocument.getDataRecord();

            this.parseDataRecord(dataRecord, "");
        } catch (XmlException | IOException ex) {
            Logger.getLogger(BHML.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Method description
     *
     *
     * @param dataRecord
     * @param tab
     */
    private void parseDataRecord(DataRecordType dataRecord, String tab) {
        DataRecordType.Field[] fieldArray = dataRecord.getFieldArray();

        for (DataRecordType.Field field : fieldArray) {
            AbstractDataComponentType abstractDataComponent = field.getAbstractDataComponent();

            if (!field.isNil() && (abstractDataComponent != null)) {
                String schemaType = abstractDataComponent.schemaType().getName().getLocalPart();

                if ("DataRecordType".equals(schemaType)) {
                    this.parseDataRecord((DataRecordType) abstractDataComponent, tab + "\t");
                }
            }
        }
    }

    /**
     * <p>
     * Title:</p>
     *
     * <p>
     * Description: </p>
     *
     * Copyright (c) Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum,
     * 2019-10-27
     *
     *
     * @version 0.9, 2019-10-27
     * @author Rainer Haener
     */
    public static final class Lithology {

        /**
         * Field description
         */
        private String material;

        /**
         * Constructs ...
         *
         */
        public Lithology() {
        }

        /**
         * Method description
         *
         *
         * @param material lithology of material
         */
        private void setMaterial(final String material) {
            this.material = material;
        }
    }
}

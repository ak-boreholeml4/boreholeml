/*
 * Copyright (C) 2023
 * Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; even without the implied WARRANTY OF MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program (see LICENSE.txt). If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA or
 * visit the Free Software Foundation's web page, http://www.fsf.org.
 */
package de.gfz_potsdam.cegit.xml;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * <p>
 * Title:</p>
 *
 * <p>
 * Description: </p>
 *
 * Copyright (c) Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum,
 * 2016-12-07
 *
 *
 * @version 1.0, 2016-12-07
 * @author Stephan Herrnkind <smueller@gfz-potsdam.de>
 * @author Rainer Haener <rainer.haener@gfz-potsdam.de>
 */
public final class BMLNamespaces {

    /**
     * Field description
     */
    public static final String BML_30_NS_PREFIX = "bml";

    /**
     * Field description
     */
    public static final String BML_30_NS = "http://www.infogeo.de/boreholeml/3.0";

    /**
     * Field description
     */
    public static final String BML_41_NS_PREFIX = "bml";

    /**
     * Field description
     */
    public static final String BML_41_NS = "http://www.gfz-potsdam.de/boreholeml/4.1";

    /**
     * ISO 19139
     */
    private static final String ISO = "http://www.isotc211.org/2005/";

    /**
     * OGC
     */
    private static final String OPENGIS = "http://www.opengis.net/";

    /**
     * Field description
     */
    private static final String VER_00 = "0.0";

    /**
     * Field description
     */
    private static final String VER_10 = "1.0";

    /**
     * Field description
     */
    private static final String VER_100 = "1.0.0";

    /**
     * Field description
     */
    private static final String VER_101 = "1.0.1";

    /**
     * Field description
     */
    private static final String VER_11 = "1.1";

    /**
     * Field description
     */
    private static final String VER_4 = "4.0.1";

    /**
     * GML 3.2.1
     */
    private static final String VER_32 = "3.2";

    /**
     * Field description
     */
    public static final String GITEWS_NS_PREFIX = "gitews";

    /**
     * Field description
     */
    public static final String GML_NS_PREFIX = "gml";

    /**
     * Field description
     */
    public static final String GML_NS = OPENGIS + GML_NS_PREFIX + "/" + VER_32;

    /**
     * Field description
     */
    public static final String OM_NS_PREFIX = "om";

    /**
     * Field description
     */
    public static final String OM_NS = OPENGIS + OM_NS_PREFIX + "/" + VER_10;

    /**
     * Field description
     */
    public static final String SA_NS_PREFIX = "sa";

    /**
     * Field description
     */
    public static final String SA_NS = OPENGIS + "sampling/" + VER_10;

    /**
     * Field description
     */
    public static final String SWE_NS_PREFIX = "swe";

    /**
     * Field description
     */
    public static final String SWE_NS = OPENGIS + SWE_NS_PREFIX + "/" + VER_101;

    /**
     * Field description
     */
    public static final String OWS_NS_PREFIX = "ows";

    /**
     * Field description
     */
    public static final String OWS_NS = OPENGIS + OWS_NS_PREFIX + "/" + VER_11;

    /**
     * Field description
     */
    public static final String SML_NS_PREFIX = "sml";

    /**
     * Field description
     */
    public static final String SML_NS = OPENGIS + "sensorML/" + VER_101;

    /**
     * Field description
     */
    public static final String OGC_NS_PREFIX = "ogc";

    /**
     * Field description
     */
    public static final String OGC_NS = OPENGIS + OGC_NS_PREFIX;

    /**
     * Field description
     */
    public static final String SENSORML_VERSION = VER_101;

    /**
     * Field description
     */
    public static final String SOS_SERVICE = "SOS";

    /**
     * Field description
     */
    private static final String RF_XML = "text/xml";

    /**
     * Field description
     */
    private static final String RF_XML_ST = RF_XML + ";subtype=\"";

    /**
     * Field description
     */
    public static final String SRS_NAME_PREFIX = "urn:ogc:def:crs:EPSG:";

    /**
     * Field description
     */
    public static final String SRS_NAME = SRS_NAME_PREFIX + "4326";

    /**
     * Field description
     */
    public static final String SAMPLING_TIME = "urn:ogc:data:time:iso8601";

    /**
     * Field description
     */
    public static final String DATE_TIME_FORMAT_RELAXED = "yyyy-MM-dd'T'HH:mm:ss";

    /**
     * Constructs ...
     *
     */
    private BMLNamespaces() {
    }

    /**
     * Method description
     *
     *
     * @return
     */
    public static DateFormat createDateFormatRelaxed() {
        return new SimpleDateFormat(DATE_TIME_FORMAT_RELAXED);
    }
}

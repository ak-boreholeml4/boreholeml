/*
 * Copyright (C) 2023
 * Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; even without the implied WARRANTY OF MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program (see LICENSE.txt). If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA or
 * visit the Free Software Foundation's web page, http://www.fsf.org.
 */

package de.gfz_potsdam.cegit.xml;

/**
 * <p>
 * Title:</p>
 *
 * <p>
 * Description: </p>
 *
 * Copyright (c) Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum,
 * 2019-07-12
 *
 *
 * @version 0.9, 2019-07-12
 * @author Rainer Haener
 */
public class EposNamespaces {

	/**
	 * SKOS namespace
	 */
	public static final String SKOS_NS = "http://www.w3.org/2004/02/skos/core#";

	/**
	 * SKOS prefix
	 */
	public static final String SKOS_NS_PREFIX = "skos";

	/**
	 * HYDRA namespace
	 */
	public static final String HYDRA_NS = "http://www.w3.org/ns/hydra/core#";

	/**
	 * HYDRA prefix
	 */
	public static final String HYDRA_NS_PREFIX = "hydra";

	/**
	 * FOAF namespace
	 */
	public static final String SCHEMA_NS = "http://schema.org/";

	/**
	 * FOAF prefix
	 */
	public static final String SCHEMA_NS_PREFIX = "schema";

	/**
	 * xmlns:dct="http://purl.org/dc/terms/"
	 */
	public static final String DCT_NS = "http://purl.org/dc/terms/";

	/**
	 * Field description
	 */
	public static final String DCT_NS_PREFIX = "dct";

	/**
	 * FOAF version
	 */
	private static final String VER_01 = "0.1";

	/**
	 * FOAF
	 */
	private static final String FOAF = "http://xmlns.com/foaf/";

	/**
	 * FOAF namespace
	 */
	public static final String FOAF_NS = FOAF + VER_01 + "/";

	/**
	 * FOAF prefix
	 */
	public static final String FOAF_NS_PREFIX = "foaf";

	/**
	 * EPOS
	 */
	public static final String EPOS_NS = "https://www.epos-eu.org/epos-dcat-ap#";

	/**
	 * EPOS prefix
	 */
	public static final String EPOS_NS_PREFIX = "epos";

	/**
	 * xmlns:eposap="http://www.epos-ip.org/"
	 */
	public static final String EPOSAP_NS = "http://www.epos-ip.org/";

	/**
	 * Field description
	 */
	public static final String EPOSAP_NS_PREFIX = "eposap";

	/**
	 * VCARD
	 */
	public static final String VCARD_NS = "http://www.w3.org/2006/vcard/ns#";

	/**
	 * Field description
	 */
	public static final String VCARD_NS_PREFIX = "vcard";

	/**
	 * xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	 */
	public static final String RDFS_NS = "http://www.w3.org/2000/01/rdf-schema#";

	/**
	 * Field description
	 */
	public static final String RDFS_NS_PREFIX = "rdfs";

	/**
	 * xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	 */
	public static final String RDF_NS = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";

	/**
	 * Field description
	 */
	public static final String RDF_NS_PREFIX = "rdf";

	/**
	 * xmlns:dcat="http://www.w3.org/ns/dcat#"
	 */
	public static final String DCAT_NS = "http://www.w3.org/ns/dcat#";

	/**
	 * Field description
	 */
	public static final String DCAT_NS_PREFIX = "dcat";

	/**
	 * Field description
	 */
	public static final String LOCN_NS = "http://www.w3.org/ns/locn#";

	/**
	 * Field description
	 */
	public static final String ADMS_NS = "http://www.w3.org/ns/adms#";

	/**
	 * Field description
	 */
	public static final String CNT_NS = "http://www.w3.org/2008/content#";

	/**
	 * Field description
	 */
	public static final String CNT_NS_PREFIX = "cnt";
}

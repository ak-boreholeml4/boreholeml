/*
 * Copyright (C) 2023
 * Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; even without the implied WARRANTY OF MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program (see LICENSE.txt). If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA or
 * visit the Free Software Foundation's web page, http://www.fsf.org.
 */
package de.gfz_potsdam.cegit.xml;

import java.util.ArrayList;
import java.util.List;

/**
 * Exception class allowing the registration of multiple exception causes and
 * providing a method to generate ows:ExceptionReport documents.
 *
 * @author Stephan Herrnkind <smueller@gfz-potsdam.de>
 */
public final class GFZOwsException extends Exception implements INamspaces {

    /**
     * Field description
     */
    private final List<ExceptionType> exceptionList = new ArrayList<>();

    /**
     * Standard constructor. Should only be used if several exception causes
     * shall be registered.
     */
    public GFZOwsException() {
        super();
    }

    /**
     * Constructor with message and cause as parameters.
     *
     * @param message String containing the message of this exception
     */
    public GFZOwsException(String message) {
        super(message);
        addCodedException(ExceptionCode.NO_APPLICABLE_CODE, null, message);
    }

    /**
     * Constructor with cause as parameter.
     *
     * @param cause Throwable cause of this exception
     */
    public GFZOwsException(Throwable cause) {
        super(cause);
        addCodedException(ExceptionCode.NO_APPLICABLE_CODE, null, cause.getMessage());
    }

    /**
     * Constructor which automatically adds one coded exception containing the
     * specified code, locator and a default exception message for the code.
     *
     * @param code Exception code of the exception to add
     * @param locator String locator of the exception to add
     */
    public GFZOwsException(ExceptionCode code, String locator) {
        super();
        addCodedException(code, locator, getExceptionCodeMessage(code));
    }

    /**
     * Constructor with message and cause as parameters.
     *
     * @param message String containing the message of this exception
     * @param cause Throwable cause of this exception
     */
    public GFZOwsException(String message, Throwable cause) {
        super(message, cause);
        addCodedException(ExceptionCode.NO_APPLICABLE_CODE, null, message + ": " + cause.getMessage());
    }

    /**
     * Constructor which automatically adds one coded exception containing the
     * specified code, locator and message.
     *
     * @param code Exception code of the exception to add
     * @param locator String locator of the exception to add
     * @param message String message of the exception to add
     */
    public GFZOwsException(ExceptionCode code, String locator, String message) {
        super();
        addCodedException(code, locator, message);
    }

    /**
     * Enum description
     *
     */
    public enum ExceptionCode {

        /**
         * Request is for an operation that is not supported by this server.
         * locator: Name of missing parameter
         */
        OPERATION_NOT_SUPPORTED("OperationNotSupported"),
        /**
         * Operation request does not include a parameter value, and this server
         * did not declare a default value for that parameter. locator: Name of
         * parameter with invalid value
         */
        MISSING_PARAMETER_VALUE("MissingParameterValue"),
        /**
         * Operation request contains an invalid parameter. locator: empty
         */
        INVALID_PARAMETER_VALUE("InvalidParameterValue"),
        /**
         * List of versions in 'AcceptVersions' parameter value in
         * GetCapabilities operation request did not include any version
         * supported by this server. locator: empty
         */
        VERSION_NEGOTIATION_FAILED("VersionNegotiationFailed"),
        /**
         * Value of (optional) 'updateSequence' parameter in GetCapabilities
         * operation request is greater than current value of service metadata
         * updateSequence number. locator: empty
         */
        INVALID_UPDATE_SEQUENCE("InvalidUpdateSequence"),
        /**
         * No other exceptionCode specified by this service and server applies
         * to this exception. locator: empty
         */
        NO_APPLICABLE_CODE("NoApplicableCode"),
        /**
         * The request a client sent to initiate an operation does not conform
         * to the schema for this operation. locator: Error message from the
         * schema-validator
         */
        INVALID_REQUEST("InvalidRequest"),
        /**
         * SensorID that has been issued by the client is not known to the
         * service. locator: Comma-separated list of the unknown sensorID(s)
         */
        UNKNOWN_SENSOR_ID("UnknownSensorID"),
        /**
         * FeasibilityID that has been issued by the client is no longer
         * supported by the service. locator: empty
         */
        FEASIBILITY_ID_EXPIRED("FeasibilityIDExpired"),
        /**
         * TaskID that has been issued by the client is no longer supported by
         * the service. locator: empty
         */
        TASK_ID_EXPIRED("TaskIDExpired");

        /**
         * Field description
         */
        private final String name;

        /**
         * Constructs ...
         *
         *
         * @param name
         */
        ExceptionCode(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return this.name;
        }
    }

    /**
     * Method description
     *
     *
     * @param code
     *
     * @return
     */
    public static String getExceptionCodeMessage(ExceptionCode code) {
        String message = null;

        switch (code) {
            case OPERATION_NOT_SUPPORTED:
                message = "Request for an operation which is not " + "supported by this server";

                break;

            case MISSING_PARAMETER_VALUE:
                message = "Operation request does not include a parameter " + "value, and this server does not declare a default " + "value for that parameter";

                break;

            case INVALID_PARAMETER_VALUE:
                message = "Operation request contains an invalid parameter";

                break;

            case VERSION_NEGOTIATION_FAILED:
                message = "List of versions in 'AcceptVersions' parameter " + "value in GetCapabilities operation request did not " + "include any version supported by this server";

                break;

            case INVALID_REQUEST:
                message = "Request does not conform to the schema";

                break;

            case UNKNOWN_SENSOR_ID:
                message = "SensorID is not known to this service";

                break;

            case FEASIBILITY_ID_EXPIRED:
                message = "FeasibilityID is no longer supported by this " + "service";

                break;

            case TASK_ID_EXPIRED:
                message = "TaskID is no longer supported by this service";

                break;

            default:
                message = "No other exceptionCode specified by this service " + "and server applies";

                break;

            // locator: Name of parameter with invalid value
        }

        return message;
    }

    /**
     * Adds a coded Exception with ExceptionCode, locator and a single String
     * message to this exception.
     *
     * @param code Exception code of the exception to add
     * @param locator String locator of the exception to add
     * @param message String message of the exception to add
     */
    public void addCodedException(ExceptionCode code, String locator, String message) {
        ExceptionType exceptionT = new ExceptionType();

        exceptionT.setExceptionCode(code.toString());

        if (locator != null) {
            exceptionT.setLocator(locator);
        }

        exceptionT.addExceptionText(message);
        this.exceptionList.add(exceptionT);
    }

    @Override
    public String getMessage() {
        StringBuilder message = new StringBuilder();

        message.append(super.getMessage());

        if (this.exceptionList.size() > 0) {
            message.append(", list of exception causes:");

            for (ExceptionType exceptionT : this.exceptionList) {
                message.append("\n  [code:" + exceptionT.getExceptionCode());
                message.append(",locator:" + exceptionT.getLocator());
                message.append("message:" + exceptionT.getExceptionTextArray(0) + "]");
            }
        }

        return message.toString();
    }

    /**
     * <p>
     * Title:</p>
     *
     * <p>
     * Description: </p>
     *
     * Copyright (c) Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum,
     * 2016-09-15
     *
     *
     * @version 0.9, 2016-09-15
     * @author Rainer Haener
     */
    public static class ExceptionType {

        /**
         * Field description
         */
        private final List<String> textArray = new ArrayList<>();

        /**
         * Field description
         */
        private String code;

        /**
         * Field description
         */
        private String locator;

        /**
         * Method description
         *
         *
         * @param code
         */
        private void setExceptionCode(String code) {
            this.code = code;
        }

        /**
         * Method description
         *
         *
         * @return
         */
        private String getExceptionCode() {
            return code;
        }

        /**
         * Method description
         *
         *
         * @param locator
         */
        private void setLocator(String locator) {
            this.locator = locator;
        }

        /**
         * Method description
         *
         *
         * @param message
         */
        private void addExceptionText(String message) {
            this.textArray.add(message);
        }

        /**
         * Method description
         *
         *
         * @return
         */
        private String getLocator() {
            return this.locator;
        }

        /**
         * Method description
         *
         *
         * @param i
         *
         * @return
         */
        private String getExceptionTextArray(int i) {
            return this.textArray.get(i);
        }
    }
}

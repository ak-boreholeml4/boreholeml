/*
 * Copyright (C) 2023
 * Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; even without the implied WARRANTY OF MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program (see LICENSE.txt). If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA or
 * visit the Free Software Foundation's web page, http://www.fsf.org.
 */

package de.gfz_potsdam.cegit.xml;

import de.gfz_potsdam.cegit.xml.GFZOwsException.ExceptionCode;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlError;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlTokenSource;

/**
 * <p>
 * Title:</p>
 *
 * <p>
 * Description: </p>
 *
 * Copyright (c) Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum,
 * 2016-12-07
 *
 *
 * @version 1.0, 2016-12-07
 * @author Stephan Herrnkind <smueller@gfz-potsdam.de>
 * @author Rainer Haener <rainer.haener@gfz-potsdam.de>
 */
public class Utils {

	/**
	 * Constructs ...
	 *
	 */
	private Utils() {}

	/**
	 * Method description
	 *
	 *
	 * @param localName
	 *
	 * @return
	 */
	public static QName createBMLQName(final String localName) {
		return new QName(BMLNamespaces.BML_30_NS_PREFIX, localName, BMLNamespaces.BML_30_NS_PREFIX);
	}

	/**
	 * Method description
	 *
	 *
	 * @param localName
	 *
	 * @return
	 */
	public static QName createGMLQName(final String localName) {
		return new QName(Namespaces.GML_NS, localName, Namespaces.GML_NS_PREFIX);
	}

	/**
	 * Method description
	 *
	 *
	 * @param localName
	 *
	 * @return
	 */
	public static QName createOMQName(final String localName) {
		return new QName(Namespaces.OM_NS, localName, Namespaces.OM_NS_PREFIX);
	}

	/**
	 * Method description
	 *
	 *
	 * @param localName
	 *
	 * @return
	 */
	public static QName createSAQName(final String localName) {
		return new QName(Namespaces.SA_NS, localName, Namespaces.SA_NS_PREFIX);
	}

	/**
	 * Method description
	 *
	 *
	 * @param localName
	 *
	 * @return
	 */
	public static QName createSPSQName(final String localName) {
		return new QName(Namespaces.SPS_NS, localName, Namespaces.SPS_NS_PREFIX);
	}

	/**
	 * Method description
	 *
	 *
	 * @param localName
	 *
	 * @return
	 */
	public static QName createSWEQName(final String localName) {
		return new QName(Namespaces.SWE_NS, localName, Namespaces.SWE_NS_PREFIX);
	}

	/**
	 * Method description
	 *
	 *
	 * @param localName
	 *
	 * @return
	 */
	public static QName createOWSQName(final String localName) {
		return new QName(Namespaces.OWS_NS, localName, Namespaces.OWS_NS_PREFIX);
	}

	/**
	 * Method description
	 *
	 *
	 * @param localName
	 *
	 * @return
	 */
	public static QName createSOSQName(final String localName) {
		return new QName(Namespaces.SOS_NS, localName, Namespaces.SOS_NS_PREFIX);
	}

	/**
	 * Method description
	 *
	 *
	 * @param localName
	 *
	 * @return
	 */
	public static QName createSMLQName(final String localName) {
		return new QName(Namespaces.SML_NS, localName, Namespaces.SML_NS_PREFIX);
	}

	/**
	 * Method description
	 *
	 *
	 * @param localName
	 *
	 * @return
	 */
	public static QName createWNSQName(final String localName) {
		return new QName(Namespaces.WNS_NS, localName, Namespaces.WNS_NS_PREFIX);
	}

	/**
	 * Method description
	 *
	 *
	 * @param localName
	 *
	 * @return
	 */
	public static QName createSASQName(final String localName) {
		return new QName(Namespaces.SAS_NS, localName, Namespaces.SAS_NS_PREFIX);
	}

	/**
	 * Method description
	 *
	 *
	 * @param localName
	 *
	 * @return
	 */
	public static QName createOGCQName(final String localName) {
		return new QName(Namespaces.OGC_NS, localName, Namespaces.OGC_NS_PREFIX);
	}

	/**
	 * Method description
	 *
	 *
	 * @return
	 */
	public static XmlOptions getNamespaceXMLOptions() {
		final XmlOptions          xmlOptions = new XmlOptions();
		final Map<String, String> prefixes   = new HashMap<>();

		// xmlOptions.setSaveAggressiveNamespaces();
		xmlOptions.setSaveNamespacesFirst();

		// Workaround for xmlns:xlink prefix, cause in any schema the name of
		// the xlink prefix is 'xlin', so set this to xlink!
		prefixes.put("http://www.w3.org/1999/xlink", "xlink");
		prefixes.put(Namespaces.GML_NS, Namespaces.GML_NS_PREFIX);
		prefixes.put(Namespaces.OM_NS, Namespaces.OM_NS_PREFIX);
		prefixes.put(Namespaces.SA_NS, Namespaces.SA_NS_PREFIX);
		prefixes.put(Namespaces.SPS_NS, Namespaces.SPS_NS_PREFIX);
		prefixes.put(Namespaces.SWE_NS, Namespaces.SWE_NS_PREFIX);
		prefixes.put(Namespaces.OWS_NS, Namespaces.OWS_NS_PREFIX);
		prefixes.put(Namespaces.SOS_NS, Namespaces.SOS_NS_PREFIX);
		prefixes.put(Namespaces.SML_NS, Namespaces.SML_NS_PREFIX);
		prefixes.put(Namespaces.WNS_NS, Namespaces.WNS_NS_PREFIX);
		prefixes.put(EposNamespaces.SCHEMA_NS, EposNamespaces.SCHEMA_NS_PREFIX);
		prefixes.put(EposNamespaces.VCARD_NS, EposNamespaces.VCARD_NS_PREFIX);
		prefixes.put(EposNamespaces.FOAF_NS, EposNamespaces.FOAF_NS_PREFIX);
		prefixes.put(EposNamespaces.CNT_NS, EposNamespaces.CNT_NS_PREFIX);
		prefixes.put(EposNamespaces.DCT_NS, EposNamespaces.DCT_NS_PREFIX);
		prefixes.put(EposNamespaces.DCAT_NS, EposNamespaces.DCAT_NS_PREFIX);
		prefixes.put(EposNamespaces.EPOS_NS, EposNamespaces.EPOS_NS_PREFIX);
		prefixes.put(EposNamespaces.HYDRA_NS, EposNamespaces.HYDRA_NS_PREFIX);
		prefixes.put(EposNamespaces.RDF_NS, EposNamespaces.RDF_NS_PREFIX);
		prefixes.put(EposNamespaces.RDFS_NS, EposNamespaces.RDFS_NS_PREFIX);
		prefixes.put(EposNamespaces.SKOS_NS, EposNamespaces.SKOS_NS_PREFIX);

		// Register, ISO19139
		prefixes.put("http://www.isotc211.org/2005/grg", "grg");
		prefixes.put("http://www.isotc211.org/2005/gsr", "gsr");
		prefixes.put("http://www.isotc211.org/2005/gss", "gss");
		prefixes.put("http://www.isotc211.org/2005/gts", "gts");
		prefixes.put("http://www.isotc211.org/2005/gmd", "gmd");
		prefixes.put("http://www.isotc211.org/2005/gco", "gco");
		prefixes.put("http://www.isotc211.org/2005/gmx", "gmx");
		xmlOptions.setSaveSuggestedPrefixes(prefixes);
		xmlOptions.setSaveImplicitNamespaces(prefixes);

		return xmlOptions;
	}

	/**
	 * Method description
	 *
	 *
	 * @param tokenSource
	 * @param outputStream
	 *
	 * @throws IOException
	 */
	public static void saveDocumentToStream(XmlTokenSource tokenSource, OutputStream outputStream) throws IOException {
		saveDocumentToStream(tokenSource, outputStream, false, true);
	}

	/**
	 * Method description
	 *
	 *
	 * @param tokenSource
	 * @param outputStream
	 * @param prettyPrint
	 *
	 * @throws IOException
	 */
	public static void saveDocumentToStream(XmlTokenSource tokenSource, OutputStream outputStream, boolean prettyPrint) throws IOException {
		saveDocumentToStream(tokenSource, outputStream, prettyPrint, true);
	}

	/**
	 * Method description
	 *
	 *
	 * @param tokenSource
	 * @param outputStream
	 * @param prettyPrint
	 * @param saveAggressiveNamespaces
	 *
	 * @throws IOException
	 */
	public static void saveDocumentToStream(XmlTokenSource tokenSource, OutputStream outputStream, boolean prettyPrint, boolean saveAggressiveNamespaces) throws IOException {
		if (tokenSource == null) {
			throw new IOException("Invalid document parameter (null)");
		}

		XmlOptions xmlOptions = getNamespaceXMLOptions();

		if (prettyPrint == true) {
			xmlOptions.setSavePrettyPrint();
		}

		if (saveAggressiveNamespaces == true) {
			xmlOptions.setSaveAggressiveNamespaces();
		}

		tokenSource.save(outputStream, xmlOptions);
	}

	/**
	 * Method description
	 *
	 *
	 * @param tokenSource
	 *
	 * @return
	 *
	 * @throws IOException
	 */
	public static String saveDocumentToString(XmlTokenSource tokenSource) throws IOException {
		return saveDocumentToString(tokenSource, false);
	}

	/**
	 * Method description
	 *
	 *
	 * @param tokenSource
	 * @param prettyPrint
	 *
	 * @return
	 *
	 * @throws IOException
	 */
	public static String saveDocumentToString(XmlTokenSource tokenSource, boolean prettyPrint) throws IOException {
		return saveDocumentToString(tokenSource, prettyPrint, true);
	}

	/**
	 * Method description
	 *
	 *
	 * @param tokenSource
	 * @param prettyPrint
	 * @param saveAggressiveNamespaces
	 *
	 * @return
	 *
	 * @throws IOException
	 */
	public static String saveDocumentToString(XmlTokenSource tokenSource, boolean prettyPrint, boolean saveAggressiveNamespaces) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		saveDocumentToStream(tokenSource, baos, prettyPrint, saveAggressiveNamespaces);

		return baos.toString();
	}

	/**
	 * Method description
	 *
	 *
	 * @param doc
	 *
	 * @throws GFZOwsException
	 */
	public static void validateRequest(XmlObject doc) throws GFZOwsException {
		List<XmlError> errorList  = null;
		XmlOptions     xmlOptions = null;

		// parse the request
		if (doc == null) {
			throw new GFZOwsException(ExceptionCode.INVALID_REQUEST, "invalid xml document (null)", "invalid xml document (null)");
		}

		// validate the request
		errorList  = new ArrayList<XmlError>();
		xmlOptions = new XmlOptions();
		xmlOptions.setErrorListener(errorList);

		if (doc.validate(xmlOptions) == false) {
			GFZOwsException owsE    = new GFZOwsException();
			String          locator = null;

			for (XmlError xmlErr : errorList) {
				locator = "Parser message: " + xmlErr.getMessage() + "\n";
				locator += "Cursor location: " + xmlErr.getCursorLocation().xmlText();
				owsE.addCodedException(ExceptionCode.INVALID_REQUEST, locator, "The request does not conform to the schema");
			}

			throw owsE;
		}
	}
}

package de.gfz_potsdam.de.id2.bml;

/*-
 * #%L
 * BoreholeML
 * %%
 * Copyright (C) 2023 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum GFZ
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */


import de.infogeo.boreholeml.x30.BoreholeDocument;

import java.io.IOException;
import java.io.InputStream;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.xmlbeans.XmlException;

/**
 * <p>
 * Title:</p>
 *
 * <p>
 * Description: </p>
 *
 * Copyright (c) Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum,
 * 2023-03-23
 *
 *
 * @version 0.9, 2023-03-23
 * @author Rainer Haener
 */
public class Borehole {

    /**
     * Manually invoke validation of a Borehole Document
     *
     *
     * @param args
     */
    public static void main(String[] args) {
        Borehole borehole = new Borehole();
        Optional<BoreholeDocument> boreholeDocumentOption = borehole.init();

        if (boreholeDocumentOption.isPresent()) {
            borehole.validate(boreholeDocumentOption.get());
        }
    }

    /**
     * Loads a Borehole Document from resources
     *
     * @return
     */
    public Optional<BoreholeDocument> init() {
        BoreholeDocument boreholeDocument = null;

        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("samples/nrw_dabo18018.xml")) {
            boreholeDocument = BoreholeDocument.Factory.parse(inputStream);
        } catch (IOException | XmlException ex) {
            Logger.getLogger(Borehole.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Optional.ofNullable(boreholeDocument);
    }

    /**
     * validates a Borehole Document
     *
     *
     * @param boreholeDocument
     *
     * @return validation result
     */
    public boolean validate(BoreholeDocument boreholeDocument) {
        boolean validate = boreholeDocument.validate();


        return validate;
    }
}

package de.gfz_potsdam.de.id2.bml;

/*-
 * #%L
 * BoreholeML
 * %%
 * Copyright (C) 2023 Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum GFZ
 * %%
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * #L%
 */

import de.infogeo.boreholeml.x30.BoreholeDocument;

import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * <p>
 * Title:BoreholeML 3.0.1 Tests</p>
 *
 * <p>
 * Description: </p>
 *
 * Copyright (c) Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum,
 * 2023-03-23
 *
 *
 * @version 0.9, 2023-03-23
 * @author Rainer Haener
 */
public class BoreholeTest {

    /**
     * Field description
     */
    Borehole instance;

    /**
     * Field description
     */
    Optional<BoreholeDocument> boreholeDocumentOption;

    /**
     * Constructs ...
     *
     */
    public BoreholeTest() {
    }

    /**
     * Method description
     *
     */
    @BeforeAll
    public static void setUpClass() {
    }

    /**
     * Method description
     *
     */
    @AfterAll
    public static void tearDownClass() {
    }

    /**
     * Method description
     *
     */
    @BeforeEach
    public void setUp() {
        instance = new Borehole();
        boreholeDocumentOption = instance.init();
    }

    /**
     * Method description
     *
     */
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of validate method, of class Borehole.
     */
    @Test
    public void testValidate() {
        System.out.println("validate");

        boolean expResult = true;
        boolean result = false;

        if (boreholeDocumentOption.isPresent()) {
            BoreholeDocument boreholeDocument = boreholeDocumentOption.get();

            result = instance.validate(boreholeDocument);
        }

        assertEquals(expResult, result);
    }
}

/*
 * Copyright (C) 2023
 * Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; even without the implied WARRANTY OF MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program (see LICENSE.txt). If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA or
 * visit the Free Software Foundation's web page, http://www.fsf.org.
 */
package de.gfz_potsdam.id2.bml.bml310;


import de.infogeo.boreholeml.x30.BoreholeDocument;
import java.io.IOException;
import java.io.InputStream;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.xmlbeans.XmlException;

/**
 * <p>
 * Title:</p>
 *
 * <p>
 * Description: </p>
 *
 * Copyright (c) Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum,
 * 2023-03-23
 *
 *
 * @version 0.9, 2023-03-23
 * @author Rainer Haener
 */
public class Borehole {

    /**
     * Manually invoke validation of a Borehole Document
     *
     *
     * @param args
     */
    public static void main(String[] args) {
        Borehole borehole = new Borehole();
        Optional<BoreholeDocument> boreholeDocumentOption = borehole.init();

        if (boreholeDocumentOption.isPresent()) {
            borehole.validate(boreholeDocumentOption.get());
        }
    }

    /**
     * Loads a Borehole Document from resources
     *
     * @return
     */
    public Optional<BoreholeDocument> init() {
        BoreholeDocument boreholeDocument = null;

        try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("samples/nrw_dabo18018.xml")) {
            boreholeDocument = BoreholeDocument.Factory.parse(inputStream);
        } catch (IOException | XmlException ex) {
            Logger.getLogger(Borehole.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Optional.ofNullable(boreholeDocument);
    }

    /**
     * validates a Borehole Document
     *
     *
     * @param boreholeDocument
     *
     * @return validation result
     */
    public boolean validate(BoreholeDocument boreholeDocument) {
        boolean validate = boreholeDocument.validate();


        return validate;
    }
}

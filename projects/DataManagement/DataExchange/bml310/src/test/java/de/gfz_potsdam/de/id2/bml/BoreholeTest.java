/*
 * Copyright (C) 2023
 * Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; even without the implied WARRANTY OF MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program (see LICENSE.txt). If not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA or
 * visit the Free Software Foundation's web page, http://www.fsf.org.
 */
package de.gfz_potsdam.de.id2.bml;

import de.gfz_potsdam.id2.bml.bml310.Borehole;

import de.infogeo.boreholeml.x30.BoreholeDocument;

import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * <p>
 * Title:BoreholeML 3.0.1 Tests</p>
 *
 * <p>
 * Description: </p>
 *
 * Copyright (c) Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum,
 * 2023-03-23
 *
 *
 * @version 0.9, 2023-03-23
 * @author Rainer Haener
 */
public class BoreholeTest {

    /**
     * Field description
     */
    Borehole instance;

    /**
     * Field description
     */
    Optional<BoreholeDocument> boreholeDocumentOption;

    /**
     * Constructs ...
     *
     */
    public BoreholeTest() {
    }

    /**
     * Method description
     *
     */
    @BeforeAll
    public static void setUpClass() {
    }

    /**
     * Method description
     *
     */
    @AfterAll
    public static void tearDownClass() {
    }

    /**
     * Method description
     *
     */
    @BeforeEach
    public void setUp() {
        instance = new Borehole();
        boreholeDocumentOption = instance.init();
    }

    /**
     * Method description
     *
     */
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of validate method, of class Borehole.
     */
    @Test
    public void testValidate() {
        System.out.println("validate");

        boolean expResult = true;
        boolean result = false;

        if (boreholeDocumentOption.isPresent()) {
            BoreholeDocument boreholeDocument = boreholeDocumentOption.get();

            result = instance.validate(boreholeDocument);
        }

        assertEquals(expResult, result);
    }
}

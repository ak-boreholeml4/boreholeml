# ![Logo of BoreholeML](https://gitlab.opencode.de/ak-boreholeml4/boreholeml/-/raw/main/resources/logo_boreholeml.png) [EN] Borehole Markup Language
## The interoperable exchange format for drilling data

BoreholeML is an XML-based exchange format for drilling information that can convey the following content:
1. Master data or metadata for the drilling (including information on the drilling location, drilling period, drilling path, drilling company, data owner, status)
2. List of layers (including information on lithology, stratigraphy, genesis)
3. Basic description of the technical drilling process (including information on the drilling process, drilling tool, mud)
4. Groundwater data during and after the drilling process
5. Evidence of sampling and log measurements
6. Expansion data (including description, date and type of installation, sectional data on diameter, material and wall thickness of the piping as well as annular space filling)
7. (Back) filling of the borehole

It was originally developed in cooperation between the German State Geological Services (SGD) and the Federal Institute for Geosciences and Natural Resources (BGR) to enable the smooth exchange of drilling data between government agencies, business and science.

## Use cases
* [Boreholemap Germany](https://boreholemap.bgr.de): Central office for the verification of all approved drilling data in Germany
* [INSPIRE: German Borehole Locations (GBL)](https://geoportal.bgr.de/mapapps/resources/apps/geoportal/index.html?lang=en#/datasets/portal/d3844378-5373-4b5f-bcf8-31ffc9d220db): Implementation of the [INSPIRE Directive 2007/2/EC](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A02007L0002-20190626) and the [Geodata Access Act] (https://www.gesetze-im-internet.de/geozg) for the interoperable provision of drilling data from Germany in accordance with the INSPIRE Geology data specification
* Data transmission in accordance with [Geology Data Act](https://www.gesetze-im-internet.de/geoldg/): Drilling data falls under the GeolDG and is a reportable project. The data obtained must be transmitted to the responsible government authorities. Some authorities in Germany (e.g. BGR, LfU) are calling for BoreholeML to be used as the data format.

## Roadmap
### Version 3.1
* Improvement of deficiencies in version 3.0.1: bug fixes and additions of important attributes
* Update of the key lists as permissible values for certain attributes based on the [Geological Mapping Guide](https://www.geokartierleitung.de/)
* Backwards compatible to version 3.0.1

### Version 4.0
* Complete redesign of the entire BoreholeML model to modernize and adapt to current ISO standards
    - ISO 19148:2021 - Geographic information — Linear referencing;
    - ISO 19156:2023 - Geographic information — Observations, measurements and samples
* Additions to the implementation of the requirements resulting from the [Geology Data Act](https://www.gesetze-im-internet.de/geoldg/) § 9
     1. Methods, data and results of borehole measurements
     2. Core data
     3. Information on sample quantities
     4. Method information for test and laboratory analyzes including their results, if applicable
     5. Results of pumping tests (including measurement data from pumping tests)
* Higher modularization
     1. Specific applications should be able to extract only data that is relevant to them,
     2. Data sets can be clearly referenced spatially, temporally and thematically
* Use of modern vocabularies (key lists) based on the Semantic Web (RDF)

## Project status
* Active
* Regular project meetings

## Installation requirements
BoreholeML (BML) is a text-based markup language that is based on the Extensible Markup Language (XML) standard and is not installed itself. BML is an open data exchange format that can be used by anyone in software for implementation without installation. BML is not an executable program or a standalone application. It is a data exchange language used to store and transmit structured drilling data.
All you need to do is write the BML code and save the BML files on a web server or locally on your computer so that others can view them via a web browser.
In order to validate the BML file, an .xsd file (XML Schema Definition) must be specified. The .xsd file is used to specify the structure and validation of Markup Language files. The BML file should reference the .xsd schema using the xmlns and xsi:schemaLocation attributes.
Here is an example:
```
    <bml>
      xmlns:bml=https://www.infogeo.de/boreholeml/4.0 xsi:schemaLocation="https://www.infogeo.de/boreholeml/4.0 https://www.infogeo.de/boreholeml/4.0/BoreholeML.xsd">
    </bml>
```

## Help / Contact
If you have any questions or comments, please contact BoreholeML@infogeo.de or start a discussion on Open CoDE. We'd love to hear from you.

## Participate
BML is developed by the Federal Geological Survey (SGD) and the Federal Institute for Geosciences and Natural Resources (BGR) and by users like you. We welcome any contribution in the form of pull requests or issues via GitLab. If you want to get access to all resources and web meetings like a fully accepted member you've to sign our "Individual" oder "Entity Contributor Exclusive License Agreement".
To get started take a look at the publication site [infogeo.de](https://www.bgr.bund.de/Infogeo/DE/Home/BoreholeML/boreholeml_node.html)  

## Author (from version 3.1)
* [Federal Institute for Geosciences and Natural Resources (BGR)](https://www.bgr.bund.de)
* [State Geological Services of Germany (SGD)](https://www.infogeo.de/)
* [The Helmholtz Center Potsdam German Research Center for Geosciences (GFZ)](https://www.gfz-potsdam.de/)
* [Leibniz Institute for Applied Geophysics (LIAG)](https://www.leibniz-liag.de/)

## License
[MIT License Copyright (c) 2023 Federal Institute for Geosciences and Natural Resources](https://gitlab.opencode.de/ak-boreholeml4/boreholeml/-/blob/main/LICENSE?ref_type=heads)

## Further Information
* [infogeo.de](https://www.bgr.bund.de/Infogeo/DE/Home/BoreholeML/boreholeml_node.html)
---

# ![Logo of BoreholeML](https://gitlab.opencode.de/ak-boreholeml4/boreholeml/-/raw/main/resources/logo_boreholeml.png) [DE] Borehole Markup Language - BoreholeML
## Das interoperable Austauschformat für Bohrdaten 

BoreholeML ist ein XML basiertes Austauschformat für Bohrungsinformationen das folgende Inhalte übermitteln kann:
1. Stammdaten oder Metadaten zur Bohrung (u.a. Angaben zur Bohrlokation, Bohrzeitraum, Bohrpfad, Bohrunternehmen, Dateninhaber, Status) 
2. Schichtenverzeichnis (u.a. Angaben zur Lithologie, Stratigraphie, Genese)
3. Grundlegende Beschreibung des technischen Bohrprozesses (u.a. Angaben zum Bohrverfahren, Bohrwerkzeug, Spülung)
4. Grundwasserdaten während und nach dem Bohrprozess
5. Nachweise über Beprobungen und Log-Messungen
6. Ausbaudaten (u.a. Beschreibung, Datum und Art des Einbaus, Abschnittsweise Daten zu Durchmesser, Material und Wandstärke der Verrohrung sowie Ringraumverfüllung)
7. (Rück-) Verfüllung der Bohrung

Es wurde ursprünglich entwickelt in Kooperation von den Staatlichen Geologischen Diensten Deutschlands (SGD) und der Bundesanstalt für Geowissenschaften und Rohstoffe (BGR), um einen reibungslosen Datenaustausch von Bohrdaten zwischen staatlichen Stellen, der Wirtschaft und der Wissenschaft zu ermöglichen. 

## Anwendungsfälle
* [Bohrpunktkarte Deutschland](https://boreholemap.bgr.de): Zentralstelle für den Nachweis aller freigegebener Bohrungsdaten in Deutschland
* [INSPIRE: Deutscher Bohrungsnachweis (GBL)](https://geoportal.bgr.de/mapapps/resources/apps/geoportal/index.html?lang=de#/datasets/portal/d3844378-5373-4b5f-bcf8-31ffc9d220db): Umsetzung der [INSPIRE-Richtline 2007/2/EG](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A02007L0002-20190626) und des [Geodatenzugangsgesetzes](https://www.gesetze-im-internet.de/geozg) für die interoperable Bereitstellung von Bohrdaten aus Deutschland gemäß der INSPIRE Datenspezifikation Geology
* Datenübermittlung gemäß [Geologiedatengesetz](https://www.gesetze-im-internet.de/geoldg/): Bohrungsdaten fallen unter das GeolDG und sind anzeigepflichtige Vorhaben. Die dabei gewonnenen Daten müssen an die zuständigen staatlichen Stellen übermittelt werden. Einige Behörden in Deutschland (z.B. BGR, LfU) fordern dazu auf BoreholeML als Datenformat zu verwenden.

## Roadmap
### Version 3.1
* Verbesserung von Defiziten in der Version 3.0.1: Fehlerkorrekturen und Ergänzungen wichtiger Attribute 
* Aktualisierung der Schlüssellisten als zulässige Werte für bestimmte Attribute auf Basis der [Geologischen Kartieranleitung](https://www.geokartieranleitung.de/)
* Abwärtskompatibel zu Version 3.0.1

### Version 4.0
* Komplettes Redesign des gesamten BoreholeML Modells zur Modernisierung und Anpassung an aktuelle ISO Normen 
   - ISO 19148:2021 - Geographic information — Linear referencing; 
   - ISO 19156:2023 - Geographic information — Observations, measurements and samples 
* Ergänzungen zur Umsetzung der Anforderungen, die sich aus dem [Geologiedatengesetz](https://www.gesetze-im-internet.de/geoldg/) § 9 ergeben
    1. Methoden, Daten und Ergebnisse von Bohrlochmessungen
    2. Bohrkerndaten
    3. Angaben zu Probemengen
    4. Methodenangaben zu Test- und Laboranalysen ggf. inkl. deren Ergebnisse
    5. Ergebnisse von Pumpversuchen (einschließlich Messdaten von Pumpversuchen)
* Höhere Modularisierung
    1. Spezifische Anwendungen sollen in die Lage versetzt werden, nur solche, für sie relevante Daten zu extrahieren,
    2. Datensätze können räumlich, zeitlich und thematisch eindeutig referenziert werden
* Nutzung moderner Vokabularien (Schlüssellisten) auf Grundlage des Semantik Webs (RDF)

## Projekt Status
* Aktiv
* Regelmäßige Projektmeetings

## Installationsanforderungen
BoreholeML (BML) ist eine textbasierte Auszeichnungssprache, die sich an den Standard Extensible Markup Language (XML) anlehnt und selbst nicht installiert wird. BML ist ein offenes Datenformat, das von jedem frei zum Datenaustausch verwendet werden kann. BML ist kein ausführbares Programm oder eine eigenständige Anwendung. Es ist eine Datenaustauschsprache, die verwendet wird, um strukturierte Bohrungsdaten zu speichern und zu übertragen.
Sie müssen lediglich den BML-Code schreiben und die BML-Dateien auf einem Webserver oder lokal auf Ihrem Computer speichern, damit andere sie über einen Webbrowser anzeigen können.
Um die BML Datei validieren zu können, muss eine .xsd-Datei (XML Schema Definition) mit angegeben werden. Die .xsd Datei wird verwendet, um die Struktur und Validierung von Markup Language Dateien festzulegen. In der BML-Datei sollte auf das .xsd-Schema verwiesen werden, indem die xmlns- und xsi:schemaLocation-Attribute verwendet werden. 
Hier ist ein Beispiel:
```
    <bml>
      xmlns:bml=https://www.infogeo.de/boreholeml/3.1 xsi:schemaLocation="https://www.infogeo.de/boreholeml/4.0 https://www.infogeo.de/boreholeml/3.1/BoreholeML.xsd">
    </bml>
```
## Hilfe / Kontakt
Bei Fragen oder Kommentaren wenden Sie sich bitte an BoreholeML@infogeo.de oder starten Sie eine Diskussion über Open CoDE. Wir würden uns freuen, von Ihnen zu hören.

## Mitmachen
BML wird von der Staatlichen Geologischen Bundesanstalt (SGD) und der Bundesanstalt für Geowissenschaften und Rohstoffe (BGR) und von Benutzern wie Ihnen entwickelt. Wir freuen uns über jeden Beitrag in Form von Pull Requests oder Issues über GitLab. Wenn sie eine vollwärtige Mitgliedschaft anstreben inkl. Teilnahme an Webmeetings, müssen sie vorher unsere "Individual" oder "Entity Contributor Exclusive License Agreement" unterzeichnen.
Zum Einstieg schauen Sie sich bitte zusätzlich die [Informationen unter infogeo.de zum Datenmodell](https://www.bgr.bund.de/Infogeo/DE/Home/BoreholeML/boreholeml_node.html) an.

## Urheber (ab Version 3.1)
* [Bundesanstalt für Geowissenschaften und Rohstoffe (BGR)](https://www.bgr.bund.de)
* [Staatliche Geologische Dienste Deutschlands (SGD)](https://www.infogeo.de/)
* [Das Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum (GFZ)](https://www.gfz-potsdam.de/)
* [Leibniz-Institut für Angewandte Geophysik (LIAG)](https://www.leibniz-liag.de/)

## Lizenz
[MIT License Copyright (c) 2023 Bundesanstalt für Geowissenschaften und Rohstoffe](https://gitlab.opencode.de/ak-boreholeml4/boreholeml/-/blob/main/LICENSE?ref_type=heads)

## Weitere Informationen
* [infogeo.de](https://www.bgr.bund.de/Infogeo/DE/Home/BoreholeML/boreholeml_node.html)
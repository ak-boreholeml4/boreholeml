TI- Statusreport (1991) geothermische Energie Deutschland/Status report 1991; geothermal energy in Germany
AU- Schulz, Ruediger
AF- Nieders. Landesamt Bodenforsch., Hanover, Federal Republic of Germany
JN- Geothermische Energie
SO- Geothermische Energie, 1992, Vol. 1992, Issue 1, pp. 4-7
PU- Geothermische Verein. : Neubrandenburg, Federal Republic of Germany
CY- Federal Republic of Germany
PD- 1992
LA- German
ES- (29B) Economic geology, economics of energy sources
SU- Central Europe; Europe; geothermal energy; Germany; hot dry rocks; research; utilization
NR- 8
PT- Serial
CD- #03740
RS- GeoRef, Copyright 2005, American Geological Institute. Reference includes data from Geoline, Bundesanstalt fur Geowissenschaften und Rohstoffe
UC- 199414
AN- 1994-031052
UR- http://search.epnet.com/login.aspx?direct=true&db=geh&an=1994-031052

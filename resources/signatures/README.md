# [EN] Petrographic signatures to represent the drilling profile

The signatures are used to represent petrographic designations of rock components in the layer sequence of a drilling profile. They correspond to the drawing regulations of DIN 4023: Geotechnical exploration and investigation - Drawing representation of the results of drillings and other direct exposures. They are assigned to the individual petrographic designations in the ["Geologische Kartieranleitung"](https://www.geokartieranleitung.de/) and partly adopted from the code lists of the BoreholeML.

## Sub directory /PNG 
The signatures under /PNG are referenced in BoreholeML 3.0.1 and the associated codelist [RockNameList v1.2 - <bmlcl:signaturePath>](https://www.infogeo.de/boreholeml/3.0/codelists/RockNameList.xml) and used for the layer representation in the [Borehole Map of Germany](https://boreholemap.bgr.de). They are stored in raster format (*.png) and therfore not very scalable.

## Sub directory /SVG 
The file names under SVG carry the codes of the petrographic designations from the [Geological Mapping Guide](https://www.geokartieranleitung.de/desktopmodules/gkalist/api/be60cbed-6fa4-494c-8a65-8778f06393159) and are defined there. They are much more detailed than the generalized signatures under /PNG for visualisation purposes in BoreholeML. They are stored in vector format (*.svg) and scale well.

--- 

# [DE]Petrographische Signaturen zur Darstellung des Bohrprofils

Die Signaturen dienen der Darstellung von petrographischer Bezeichnungen von Gesteinskomponten in der Schichtabfolge eines Bohrprofils. Sie entsprechen der Zeichnvorschriften der DIN 4023: Geotechnische Erkundung und Untersuchung - Zeichnerische Darstellung der Ergebnisse von Bohrungen und sonstigen direkten Aufschlüssen. Sie werden in der ["Geologischen Kartieranleitung"](https://www.geokartieranleitung.de/) den einzelnen petrographischen Bezeichnungen zugordnet und teilweise von den Codelisten der BoreholeML übernommen.

## Unterverzeichnis /PNG 
Die Signaturen unter /PNG werden in BoreholeML 3.0.1 und der dazugehörigen Codelist [RockNameList v1.2 - <bmlcl:signaturePath>](https://www.infogeo.de/boreholeml/3.0/codelists/RockNameList.xml) referenziert und für die Schichtdarstellung in der [Bohrpunktkarte Deutschland](https://boreholemap.bgr.de) verwendet. Sie sind im Rasterformat (*.png) abgespeichert und wenig skalierbar.

## Unterverzeichnis /SVG
Die Dateinamen unter SVG tragen die Codes der petrographischen Bezeichnungen aus der [Geologischen Kartieranleitung](https://www.geokartieranleitung.de/desktopmodules/gkalist/api/be60cbed-6fa4-494c-8a65-8778f06393159) und sind dort defniert. Sie sind wesentlich ausführlicher als die generalisierten Signaturen unter /PNG für die Darstellung in BoreholeML. Sie sind im Vektorformat (*.svg) abgespeichert und gut skalierbar.